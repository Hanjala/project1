<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'DefaultController@index')->name('default');
// Route::get('product/{id}', 'DefaultController@show')->name('default');

Auth::routes();


Route::group(['middleware' => ['auth'], 'prefix' => 'brands'], function () {
    Route::get('', 'BrandController@index')->name('brands.all');
    Route::post('', 'BrandController@store')->name('brands.save');
});


Route::group(['middleware' => ['auth'], 'prefix' => 'versions'], function () {
    Route::get('', 'VersionController@index')->name('versions.all');
    Route::post('', 'VersionController@store')->name('versions.save');
});


Route::group(['middleware' => ['auth'], 'prefix' => 'products'], function () {
    Route::get('', 'ProductController@index')->name('products.all');
    Route::post('', 'ProductController@store')->name('products.save');
});