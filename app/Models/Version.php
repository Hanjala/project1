<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    protected $table = 'versions';
    protected $fillable = ['title', 'description', 'brand_id', 'total'];


    public function Brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
