<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['version_id', 'price', 'user_id', 'thumb', 'images'];

    public function Version()
    {
        return $this->belongsTo(Version::class);
    }
}
