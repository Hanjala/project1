<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = ['title', 'description'];


    public function Versions()
    {
        return $this->hasMany(Version::class, 'brand_id');
    }
}
