<?php

namespace App\Helpers;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Class DiskHelper
 * @package App\Helper
 */
class DiskHelper
{
    /**
     * @param $file
     * @param $folder
     * @return array
     */
    public function UploadImageFile($file, $folder)
    {
        $img = Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
        });

        $file_name = time() . '.' . $file->getClientOriginalExtension();
        $file_name_thumb = "{$folder}/thumb/{$file_name}";


        Storage::disk('public')->put($file_name_thumb, $img->stream()->__toString());
        $original_path = Storage::disk('public')->putFileAs($folder, $file, $file_name);

        return [
            'url' => asset(Storage::url($original_path)),
            'thumb' => asset(Storage::url($file_name_thumb)),
            'width' => Image::make(Storage::disk('public')->get($original_path))->width(),
            'height' => Image::make(Storage::disk('public')->get($original_path))->height()
        ];
    }


    /**
     * @param $files
     * @param $folder
     * @return array
     */
    public function UploadImages($files, $folder)
    {
        $paths = [];
        foreach ($files as $key => $file) {
            $data = $this->UploadImageFile($file, $folder);
            sleep(1);
            $paths[] = $data;
        }

        return $paths;
    }
}