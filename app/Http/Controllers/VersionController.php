<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 6/15/18
 * Time: 5:43 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\StoreVersionRequest;
use App\Models\Brand;
use App\Models\Version;

/**
 * Class VersionController
 * @package App\Http\Controllers
 */
class VersionController extends Controller
{
    /**
     * @var Version
     */
    /**
     * @var Brand|Version
     */
    protected $version, $brand;


    /**
     * VersionController constructor.
     * @param Version $version
     * @param Brand $brand
     */
    public function __construct(Version $version, Brand $brand)
    {
        $this->version = $version;
        $this->brand = $brand;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('versions', ['versions' => $this->version->all(), 'brands' => $this->brand->all()]);
    }


    /**
     * @param StoreVersionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreVersionRequest $request)
    {
        $this->version->create($request->all());
        return redirect(route('versions.all'));
    }
}