<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 6/17/18
 * Time: 8:25 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\DefaultRequest;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Version;

/**
 * Class DefaultController
 * @package App\Http\Controllers
 */
class DefaultController extends Controller
{

    /**
     * @var Product
     */
    /**
     * @var Product|Version
     */
    protected $brand, $version;


    /**
     * DefaultController constructor.
     * @param Product $product
     * @param Brand $brand
     */
    public function __construct(Product $product, Brand $brand)
    {
        $this->product = $product;
        $this->brand = $brand;
    }


    /**
     * @param DefaultRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DefaultRequest $request)
    {

            $products = $this->product->with('version')->where(function ($query) use ($request) {
                if ($request->get('version_id'))
                    $query->where(['version_id' => $request->get('version_id')]);
                if ($request->get('from') && $request->get('to'))
                    $query->whereBetween('price', [$request->get('from'), $request->get('to')]);
                if ($request->get('from_date') && $request->get('to_date'))
                    $query->whereBetween('created_at', [date($request->get('from_date')), date($request->get('to_date'))]);
            })->get();

        return view('home', ['brands' => $this->brand->with('versions')->get(), 'products' => $products]);
    }

    public function show($id)
    {

    }
}