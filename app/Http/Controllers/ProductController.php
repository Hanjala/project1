<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 6/15/18
 * Time: 6:38 PM
 */

namespace App\Http\Controllers;


use App\Helpers\DiskHelper;
use App\Http\Requests\StoreProductRequest;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Version;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @var
     */
    protected $product, $brand, $version;


    public function __construct(Product $product, Brand $brand, Version $version)
    {
        $this->product = $product;
        $this->brand = $brand;
        $this->version = $version;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('products', ['brands' => $this->brand->with('versions')->get(), 'products' => $this->product->with('version.brand')->get()]);
    }


    /**
     * @param StoreProductRequest $request
     * @param DiskHelper $diskHelper
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreProductRequest $request, DiskHelper $diskHelper)
    {

        $inputData = $request->all();
        $imageData = $diskHelper->UploadImageFile($request->file('thumb'), 'product');
        $inputData['thumb'] = $imageData['thumb'];
        $images = $diskHelper->UploadImages($request->file('images'), 'product');
        $tempArray = [];
        foreach ($images as $image)
            $tempArray[] = $image['thumb'];
        $inputData['images'] = implode($tempArray, ',');
        $inputData['user_id'] = Auth::user()->id;
        $this->product->create($inputData);
        $this->updateVersionNumber($request->get('version_id'));
        return redirect(route('products.all'));
    }


    private function updateVersionNumber($version_id)
    {
        $version = $this->version->find($version_id);
        if ($version) {
            $version->total = $version->total + 1;
            $version->save();
        }
    }
}