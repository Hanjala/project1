@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <form method="POST" action="{{ route('versions.save') }}" aria-label="{{ __('Register') }}">
                    @csrf

                    <div class="form-group row">

                        <div class="col-md-12">
                            <input placeholder="Title" id="title" type="text"
                                   class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title"
                                   value="{{ old('title') }}" required autofocus>

                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input placeholder="Description" id="description" type="text"
                                   class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                   name="description" value="{{ old('description') }}" required>

                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-12">
                            <select name="brand_id"
                                    class="form-control{{ $errors->has('brand_id') ? ' is-invalid' : '' }}">
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->title}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('brand_id'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('brand_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="card-group">
                    @foreach($versions as $vesion)
                        <div class="col-sm-4">
                            <div class="card custom_margin">
                                <div class="card-body">
                                    <h5 class="card-title">{{$vesion->title}}</h5>
                                    <p class="card-text">{{$vesion->description}}</p>
                                    <a href="#" class="btn btn-primary">Detail</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
