@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                @foreach($brands as $brand)
                    <h3>{{$brand->title}}</h3>
                    <ul class="list-group">
                        @foreach($brand->versions as $version)
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="{{ route('default',['version_id'=>$version]) }}">
                                    {{$version->title}}
                                    <span class="badge badge-primary badge-pill">{{$version->total}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endforeach

                <hr/>
                <h3>Search area</h3>
                <form method="get" action="{{ route('default') }}">

                    <div class="form-group row">
                        <div class="col-md-12">
                            <select name="version_id"
                                    class="form-control{{ $errors->has('version_id') ? ' is-invalid' : '' }}">
                                <option value="">Select Brand</option>
                                @foreach($brands as $brand)
                                    <optgroup label="{{$brand->title}}">
                                        @foreach($brand->versions as $version)
                                            <option value="{{$version->id}}">{{$version->title}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>


                        </div>
                    </div>


                    <div class="form-group row">

                        <div class="col-md-6">
                            <input placeholder="From" id="from" type="number"
                                   class="form-control {{ $errors->has('from') ? ' is-invalid' : '' }}" name="from"
                                   value="{{ old('from') }}"  autofocus>


                        </div>
                        <div class="col-md-6">
                            <input placeholder="To" id="to" type="number"
                                   class="form-control {{ $errors->has('to') ? ' is-invalid' : '' }}" name="to"
                                   value="{{ old('to') }}"  autofocus>


                        </div>
                    </div>


                    <div class="form-group row">

                        <div class="col-md-12">
                            <label>From date</label>
                            <input placeholder="From Date" id="from_date" type="date"
                                   class="form-control {{ $errors->has('from_date') ? ' is-invalid' : '' }}" name="from_date"
                                   value="{{ old('from_date') }}"  autofocus>


                        </div>
                        <div class="col-md-12">
                            <label>To date</label>
                            <input placeholder="To date" id="to_date" type="date"
                                   class="form-control {{ $errors->has('to_date') ? ' is-invalid' : '' }}" name="to_date"
                                   value="{{ old('to_date') }}"  autofocus>


                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Search') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>
            <div class="col-md-9">
                <div class="card-group">
                    @foreach($products as $product)
                        <div class="col-sm-6">
                            <div class="card">
                                <img class="card-img-top" src="{{$product->thumb}}" alt="{{$product->version->title}}">
                                <div class="card-body">
                                    <h5 class="card-title">{{$product->version->brand->title}} {{$product->version->title}}</h5>
                                    <p class="card-text">{{$product->price}} BDT</p>
                                    <p class="card-text">{{$product->version->description}}</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
