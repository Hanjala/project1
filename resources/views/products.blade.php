@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <form method="POST" action="{{ route('products.save') }}" aria-label="{{ __('Register') }}"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input placeholder="Price" id="price" type="number"
                                   class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price"
                                   value="{{ old('price') }}" required autofocus>

                            @if ($errors->has('price'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-12">
                            <select name="version_id"
                                    class="form-control{{ $errors->has('version_id') ? ' is-invalid' : '' }}">
                                <option value="">Select Brand</option>
                                @foreach($brands as $brand)
                                    <optgroup label="{{$brand->title}}">
                                        @foreach($brand->versions as $version)
                                            <option value="{{$version->id}}">{{$version->title}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>

                            @if ($errors->has('version_id'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('version_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-12">
                            <label>Uplaod Thumb</label>
                            <input type="file" name="thumb" id="thumb" accept="image/x-png,image/gif,image/jpeg"/>

                            @if ($errors->has('thumb'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('thumb') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-12">
                            <label>Uplaod Product Images</label>
                            <input multiple type="file" name="images[]" id="images"
                                   accept="image/x-png,image/gif,image/jpeg"/>

                            @if ($errors->has('images'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <div class="card-group">
                    @foreach($products as $product)
                        <div class="col-sm-6">
                            <div class="card">
                                <img class="card-img-top" src="{{$product->thumb}}" alt="{{$product->version->title}}">
                                <div class="card-body">
                                    <h5 class="card-title">{{$product->version->brand->title}} {{$product->version->title}}</h5>
                                    <p class="card-text">{{$product->version->description}}</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
